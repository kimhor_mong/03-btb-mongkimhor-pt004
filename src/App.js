import './style/App.css';
import { Container, Row, Col } from 'react-bootstrap';
import Item from './components/Item';
import NavMenu from './components/NavMenu';

import React, { Component } from 'react';

export default class App extends Component {
	constructor() {
		super();
		this.state = {
			data: [
				{
					title: 'Card Title 1',
					img: 'https://www.remotelands.com/travelogues/app/uploads/2019/09/Angkor-Wat-1.jpg',
					text:
						" Some quick example text to build on the card title and make up the bulk of the card's content.",
					isShow: true,
				},
				{
					title: 'Card Title 2',
					img: 'https://www.remotelands.com/travelogues/app/uploads/2019/09/Angkor-Wat-1.jpg',
					text:
						" Some quick example text to build on the card title and make up the bulk of the card's content.",
					isShow: true,
				},
				{
					title: 'Card Title 3',
					img: 'https://www.remotelands.com/travelogues/app/uploads/2019/09/Angkor-Wat-1.jpg',
					text:
						" Some quick example text to build on the card title and make up the bulk of the card's content.",
					isShow: true,
				},
			],
		};
	}

	toggleCard = (index) => {
		let newData = this.state.data;
		newData[index] = !newData[index].isShow;
		this.setState({ data: newData });
	};
	render() {
		return (
			<div className='App'>
				<NavMenu />
				<Container>
					<Row>
						{this.state.data.map(
							(article, index) =>
								article.isShow && (
									<Col sm={6} md={3} key={index} className='mt-4'>
										<Item
											data={article}
											onClick={() => {
												this.toggleCard(index);
											}}
										/>
									</Col>
								)
						)}
					</Row>
				</Container>
			</div>
		);
	}
}

import React from 'react';
import { Card, Button } from 'react-bootstrap';

export default function Item({ data, onClick }) {
	let { img, title, text } = data;

	return (
		<Card>
			<Card.Img variant='top' src={img} />
			<Card.Body>
				<Card.Title>{title}</Card.Title>
				<Card.Text>{text}</Card.Text>
				<Button onClick={onClick} variant='primary'>
					Hide
				</Button>
			</Card.Body>
		</Card>
	);
}
